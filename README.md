# Master 2 HCHN: les citations d'Aristophane dans les *Deipnosophistes* d'Athénée de Naucratis



Voici le résultat de mon projet de recherches de master 2 Humanités classiques et humanités numériques, sous la direction de et en collaboration avec Aurélien Berra, à l'Université de Nanterre.

Le mémoire, disponible en pdf et html, s'accompagne du texte de Kaibel encodé, et d'un catalogue des citations que nous avons relevées.
