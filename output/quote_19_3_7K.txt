<quote xmlns="http://www.tei-c.org/ns/1.0" source="#aristophanes" rend="blockquote">
  <l>συκᾶς φυτεύω πάντα πλὴν Λακωνικῆς·</l>
  <l>τοῦτο γὰρ τὸ σῦκον ἐχθρόν ἐστι καὶ τυραννικόν.</l>
  <l>οὐ γὰρ ἦν ἂν μικρόν, εἰ μὴ μισόδημον ἦν σφόδρα.</l>
</quote>