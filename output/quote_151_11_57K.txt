<quote xmlns="http://www.tei-c.org/ns/1.0" source="#aristophanes" rend="blockquote">
  <l>ὃ δ᾽ ἀλφίτων .. πριάμενος τρεῖς χοίνικας</l>
  <l>κοτύλης δεούσας εἴκοσ᾽ ἀπολογίζεται.<pb n="v.3.p.54"/>
  </l>
</quote>