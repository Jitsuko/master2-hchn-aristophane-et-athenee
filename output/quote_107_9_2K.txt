<quote xmlns="http://www.tei-c.org/ns/1.0" source="#aristophanes" rend="blockquote">
  <l>καὶ πῶς ἐγὼ Σθενέλου φάγοιμ᾽ ἂν ῥήματα;</l>
  <l>εἰς ὄξος ἐμβαπτόμενος ἢ λευκοὺς ἅλας;</l>
</quote>