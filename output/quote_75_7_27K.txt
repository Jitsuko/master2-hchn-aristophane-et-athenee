<quote xmlns="http://www.tei-c.org/ns/1.0" source="#aristophanes" rend="blockquote">
  <l>ἀλλ᾽ ἔχουσα γαστέρα</l>
  <l>μεστὴν βοάκων ἀπεβάδιζον οἴκαδε.<pb n="v.2.p.134"/>
  </l>
</quote>