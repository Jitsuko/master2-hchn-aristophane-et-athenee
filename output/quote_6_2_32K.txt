<quote xmlns="http://www.tei-c.org/ns/1.0" source="#aristophanes" rend="blockquote">
  <l>τράπεζαν ἡμῖν<add>εἴσ</add>φερε</l>
  <l>τρεῖς πόδας ἔχουσαν, τέσσαρας δὲ μὴ ᾽χέτω.</l>
  <l>Β. καὶ πόθεν ἐγὼ τρίπουν τράπεζαν λήψομαι;</l>
</quote>