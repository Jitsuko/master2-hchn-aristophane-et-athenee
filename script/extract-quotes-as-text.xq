let $doc := doc("../data/athenaeus-kaibel-2019-06-24.xml")//*:quote[@source = "#aristophanes"]

for $q at $i in $doc
let $b := $q/ancestor::*:div[@subtype = "book"]/@n/data()
let $c := $q/ancestor::*:div[@subtype = "chapter"]/@n/data()

return
(: concat("[", $b,".", $c,"K","]"," ", $q) :)
file:write(
  concat("../output/quote_",$i,"_", $b,"_",$c,"K", ".txt"), $q)